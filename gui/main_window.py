from PySide2.QtWidgets import QMainWindow, QLabel
from PySide2.QtGui import QIcon
from PySide2.QtCore import Qt
from gui.widgets.central_widget import CentralWidget
from gui.widgets.menu_bar import MenuBar
from gui.widgets.status_bar import StatusBar
from gui.widgets.tool_bar import ToolBar
from gui.widgets.structure_dock_widget import StructureDockWidget


class MainWindow(QMainWindow):
    def __init__(self, parent=None):
        super().__init__(parent)
        self.setWindowTitle("Rukovalac informacionim resursima")
        self.setWindowIcon(QIcon("resources/icon/blue-document.png"))
        self.resize(1000, 800)
        
        self.menu_bar = MenuBar(self)
        self.tool_bar = ToolBar(parent=self)
        self.status_bar = StatusBar(self)
        self.central_widget = CentralWidget(self)
        self.file_dock_widget = StructureDockWidget("Struktura radnog prostora", self)
       
        self.setMenuBar(self.menu_bar)
        self.addToolBar(self.tool_bar)
        self.setStatusBar(self.status_bar)
        self.setCentralWidget(self.central_widget)
        self.addDockWidget(Qt.LeftDockWidgetArea, self.file_dock_widget, Qt.Vertical)

        self.status_bar.addWidget(QLabel("Ulogovani korisnik: " + "Jest Bogami"))