from PySide2.QtWidgets import QWidget, QVBoxLayout, QTextEdit
from gui.widgets.table_view.test_table_view import TestTableView

class CentralWidget(QWidget):
    def __init__(self, parent=None):
        super().__init__(parent)
        self.cw_layout = QVBoxLayout(self) 
        self.table = TestTableView()
        self.cw_layout.addWidget(self.table)
        self.setLayout(self.cw_layout)