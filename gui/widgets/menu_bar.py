from PySide2.QtWidgets import QMenuBar, QMenu

class MenuBar(QMenuBar):
    def __init__(self, parent=None):
        super().__init__(parent)

        self.file_menu = QMenu("File", self)
        self.edit_menu = QMenu("Edit", self)
        self.help_menu = QMenu("Help", self)
        self._poulate_menues()

    def _poulate_menues(self):
        self.addMenu(self.file_menu)
        self.addMenu(self.edit_menu)
        self.addMenu(self.help_menu)

    def add_actions(self, menu="file", actions=[]):
        if len(actions) == 0:
            return
        if menu == "file":
            self.file_menu.addSeparator()
            self.file_menu.addActions(actions)
        elif menu == "edit":
            self.edit_menu.addSeparator()
            self.edit_menu.addActions(actions)
        elif menu == "help":
            self.help_menu.addSeparator()
            self.help_menu.addActions(actions)