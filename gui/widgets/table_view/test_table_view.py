from PySide2.QtWidgets import QWidget, QVBoxLayout, QTableView
from gui.widgets.table_view.test_table_model import TestTableModel

class TestTableView(QWidget):
    def __init__(self, parent=None):
        super().__init__(parent)
        self.wiget_layout = QVBoxLayout()
        self.table_view = QTableView()
        self.table_model = TestTableModel(data=self.generate_dummy_data())
        self.table_view.setModel(self.table_model)
        self.wiget_layout.addWidget(self.table_view)
        self.setLayout(self.wiget_layout)

    def generate_dummy_data(self):
        data = [
            ["2312312", "Mihajlo", "Vuckovic"],
            ["2312312", "Luka", "Marinko"]  
        ]

        return data