from PySide2.QtCore import QAbstractTableModel,QModelIndex,Qt

class TestTableModel(QAbstractTableModel):
    def __init__(self, parent=None, data=None):
        super().__init__(parent)
        self.table_data = data 
        self.header_data = ["Indeks","Ime","Prezime"]
    
    def get_element(self, index: QModelIndex):
        if index.isValid():
            element = self.table_data[index.row()][index.column()]
            if element:
                return element
            return self.table_data
    
    def rowCount(self, parent=...):
        return len(self.table_data)
        
    def columnCount(self, parent=...):
        return 3
    
    def data(self, index, role=...):
        element = self.get_element(index)
        if role == Qt.DisplayRole: 
            return element 
    
    def headerData(self, section: int, orientation: Qt.Orientation, role: int = ...) :
        if orientation == Qt.Horizontal and role == Qt.DisplayRole:
            return self.header_data[section] 


