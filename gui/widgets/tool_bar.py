from PySide2.QtWidgets import QToolBar,QAction
from PySide2.QtGui import QIcon



class ToolBar(QToolBar):
    def __init__(self, title="", parent=None):
        super().__init__(title, parent)
        self.actions_dict = {} 



    def default_actions(self):
        open_action = QAction(QIcon(), "Open")
        exit_action = QAction(QIcon(), "Exit")
        help_action = QAction(QIcon(), "Help")
        about_action = QAction(QIcon(), "About")
        about_action.setStatusTip("About Program")
      
        self.actions_dict["open"] = open_action
        self.actions_dict["exit"] = exit_action
        self.actions_dict["help"] = help_action
        self.actions_dict["about"] = about_action

        self.addAction(open_action)
        self.addAction(exit_action)
        self.addAction(help_action)
        self.addAction(about_action)

    #    def bind_actions(self):
    #      self.actions_dict["about"].triggered.connect(self.on_about_action)

        
     #   def on_about_action(self):
    #     ...


