from PySide2.QtWidgets import QWidget, QVBoxLayout, QTableWidget,QTableWidgetItem


class TestTableWidget(QWidget):
    
    def __init__(self, parent=None):
        super().__init__(parent)
       
        self.widget_layout = QVBoxLayout()
        self.table_widget = QTableWidget(parent)
        self.table_widget.setColumnCount(3) 
        self.table_widget.setRowCount(10) 
        self.table_widget.setHorizontalHeaderLabels(["Indeks", "Ime", "Prezime"])
        self.fill_data() 
        self.widget_layout.addWidget(self.table_widget)
        self.setLayout(self.widget_layout) 

    def fill_data(self):
        for i in range(10):
            col1 = QTableWidgetItem("2021/270222" + str(i))
            col2 = QTableWidgetItem("Marko" + str(i))
            col3 = QTableWidgetItem("Markovic" + str(i))
        
            self.table_widget.setItem(i, 0, col1)
            self.table_widget.setItem(i, 1, col2)
            self.table_widget.setItem(i, 2, col3)

